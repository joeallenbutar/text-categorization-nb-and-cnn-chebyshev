#author : Suyash Lakhotia
#modified : Joe_Putri_CVS

import flask

import preprocess
import models
import rcv1_constants as dataset

app = flask.Flask(__name__)


@app.errorhandler(404)
def not_found():
    return flask.make_response(flask.jsonify({"error": "Resource not found!"}), 404)


@app.route('/')
def root():
    return flask.render_template("index.html")


@app.route("/classify", methods=["POST"])
def classify_text():
    req = flask.request.get_json()
    text = req.get("text")
    model_id_arr = req.get("model-checkbox")

    data_tfidf = preprocess.preprocess(text, dataset.VOCABULARY)
    results = models.run_models(model_id_arr, data_tfidf)

    _res = []
    for model_id, result in results.items():
        _res.append(_json_record(model_id, dataset.CLASS_NAMES[dataset.CLASSES[result[0]]], result[1]))

    return flask.jsonify(_res)


def _json_record(model_id, class_name, confidence):
    if confidence != "N/A":
        record = {"modelID": model_id, "className": class_name, "confidence": "{:.2f}".format(confidence)}
    else:
        record = {"modelID": model_id, "className": class_name}

    return record


if __name__ == "__main__":
    app.run(debug=True)
