#author : Suyash Lakhotia
#modified : Joe_Putri_CVS

import pickle

import numpy as np
import tensorflow as tf


import rcv1_constants as dataset

MODEL_IDS = ["multinomial_nb", "gcnn_chebyshev"]


def run_models(model_id_arr, data_tfidf):
    results = {}
    #
    if "multinomial_nb" in model_id_arr:
        results["multinomial_nb"] = run_multinomial_nb(data_tfidf)
    if "gcnn_chebyshev" in model_id_arr:
        results["gcnn_chebyshev"] = run_gcnn_chebyshev(data_tfidf)
    return results


def run_multinomial_nb(data_tfidf):
    clf = pickle.load(open("models/multinomial_nb/pickle.pkl", "rb"))
    predicted = clf.predict(data_tfidf)[0]
    confidence = clf.predict_proba(data_tfidf)
    if predicted >= 45:  # issue with 'GVOTE' class
        _predicted = predicted - 1
    else:
        _predicted = predicted
    return predicted, confidence[0][_predicted] * 100


def run_gcnn_chebyshev(data_tfidf):
    predicted, probability = _run_tf_model(data_tfidf,
                                           "models/gcnn_chebyshev/checkpoints/model-164100.meta",
                                           "models/gcnn_chebyshev/checkpoints/.")
    return predicted, probability * 100


def _run_tf_model(data, graph_filepath, checkpoints_filepath):
    with tf.Session(graph=tf.Graph()) as sess:
        saver = tf.train.import_meta_graph(graph_filepath)
        saver.restore(sess, tf.train.latest_checkpoint(checkpoints_filepath))

        scores_op = tf.get_default_graph().get_tensor_by_name("output/scores:0")
        predictions_op = tf.get_default_graph().get_tensor_by_name("output/predictions:0")

        x_batch = np.zeros((dataset.BATCH_SIZE, data.shape[1]))
        x_batch[0] = data[0]
        feed_dict = {
            "input_x:0": x_batch,
            "train_flag:0": False
        }
        scores, predictions = sess.run([scores_op, predictions_op], feed_dict)

    predicted = predictions[0]

    e_x = np.exp(scores[0] - np.max(scores[0]))
    probability = e_x[predicted] / np.sum(e_x)

    return predicted, probability
