const API_BASE_URL = 'http://127.0.0.1:5000';

const MODEL_IDS = ["multinomial_nb", "gcnn_chebyshev"]
const MODEL_NAMES = {"multinomial_nb": "Multinomial Naive Bayes", "gcnn_chebyshev": "Graph CNN (Chebyshev)"}

function submit() {
    clearResults();

    let text = $('#text').val();

    let $selectedModels = $('input[name="model-checkbox"]:checked');
    let selectedModels = [];
    for (const $modelCheckbox of $selectedModels) {
        selectedModels.push($modelCheckbox.value);
    }
    
    let request = {
        modelIDs: selectedModels,
        text: text
    };

    $('#submit-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading');
    $('#submit-btn').prop('disabled', true);

    $.ajax({
        async: true,
        crossDomain: true,
        url: API_BASE_URL + "/classify",
        method: "POST",
        headers: {
            "content-type": "application/json",
            "cache-control": "no-cache",
        },
        processData: false,
        data: JSON.stringify(request)
    }).then((data) => {
        showResults(data);
    });
}

function showResults(data) {
    let $results = $('#results');
    for (const datum of data) {
        let $result = $('#result-template').clone().removeAttr('id');
        
        let html = '<strong>' + MODEL_NAMES[datum['modelID']] + ':</strong> ' + datum['className'];
        if (datum['confidence'] != undefined)
            html += ' (' + datum['confidence'] + '%)';

        $result.html(html);
        $results.append($result);
    }
    $('#results-section').show();
    $('#submit-btn').html('Submit');
    $('#submit-btn').prop('disabled', false);
}

function clearResults() {
    $('#results .result').remove();
    $('#results-section').hide();
}
