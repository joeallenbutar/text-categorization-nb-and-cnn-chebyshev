#author : Suyash Lakhotia
#modified : Joe_Putri_CVS

import numpy as np
import scipy
import tensorflow as tf

import lib_gcnn.graph as graph


class GraphCNN(object):
    def __init__(self, filter_name, L, K, F, P, FC, batch_size, num_vertices, num_classes, l2_reg_lambda):
        # Sanity checks
        assert len(L) >= len(F) == len(K) == len(P)
        assert np.all(np.array(P) >= 1)
        p_log2 = np.where(np.array(P) > 1, np.log2(P), 0)
        assert np.all(np.mod(p_log2, 1) == 0)
        assert len(L) >= 1 + np.sum(p_log2)

        assert filter_name == "chebyshev" or filter_name == "spline" or filter_name == "fourier"
        self.graph_conv = getattr(self, "graph_conv_" + filter_name)

        self.input_x = tf.placeholder(tf.float32, [batch_size, num_vertices], name="input_x")
        self.input_y = tf.placeholder(tf.int32, [batch_size], name="input_y")
        self.train_flag = tf.placeholder(tf.bool, name="train_flag")
        self.dropout_keep_prob = tf.placeholder_with_default(1.0, shape=[], name="dropout_keep_prob")

        l2_loss = tf.constant(0.0)

        M_0 = L[0].shape[0]
        j = 0
        L_tmp = []
        for p_i in P:
            L_tmp.append(L[j])
            j += int(np.log2(p_i)) if p_i > 1 else 0
        L = L_tmp

        x = tf.expand_dims(self.input_x, 2)

        for i in range(len(K)):
            with tf.variable_scope("conv-maxpool-{}".format(i)):
                with tf.variable_scope("conv-{}-{}".format(K[i], F[i])):
                    x = self.graph_conv(x, L[i], K[i], F[i])
                    b = tf.Variable(tf.constant(0.1, shape=[1, 1, F[i]]), name="b")
                    x = tf.nn.relu(x + b, name="relu")
                with tf.variable_scope("maxpool-{}".format(P[i])):
                    x = self.graph_max_pool(x, P[i])

        with tf.variable_scope("dropout"):
            x = tf.nn.dropout(x, self.dropout_keep_prob)

        with tf.variable_scope("reshape"):
            B, V, F = x.get_shape()
            B, V, F = int(B), int(V), int(F)
            x = tf.reshape(x, [B, V * F])

        # Add fully-connected layers (if any)
        for i, num_units in enumerate(FC):
            with tf.variable_scope("fc-{}-{}".format(i, num_units)):
                W = tf.get_variable("W",
                                    shape=[x.get_shape().as_list()[1], num_units],
                                    initializer=tf.contrib.layers.xavier_initializer())
                b = tf.Variable(tf.constant(0.1, shape=[num_units]), name="b")

                l2_loss += tf.nn.l2_loss(W)

                x = tf.nn.xw_plus_b(x, W, b)
                x = tf.layers.batch_normalization(x, training=self.train_flag)
                x = tf.nn.relu(x)
                x = tf.nn.dropout(x, self.dropout_keep_prob)

        # Final (unnormalized) scores and predictions
        with tf.variable_scope("output"):
            W = tf.get_variable("W",
                                shape=[x.get_shape().as_list()[1], num_classes],
                                initializer=tf.contrib.layers.xavier_initializer())
            b = tf.Variable(tf.constant(0.1, shape=[num_classes]), name="b")

            l2_loss += tf.nn.l2_loss(W)

            self.scores = tf.nn.xw_plus_b(x, W, b, name="scores")
            self.predictions = tf.argmax(self.scores, 1, name="predictions")
            self.predictions = tf.cast(self.predictions, tf.int32)

        # Calculate mean cross-entropy loss
        with tf.variable_scope("loss"):
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.scores, labels=self.input_y)
            self.loss = tf.reduce_mean(losses) + l2_reg_lambda * l2_loss

        # Calculate accuracy
        with tf.variable_scope("accuracy"):
            correct_predictions = tf.equal(self.predictions, self.input_y)
            self.accuracy = tf.reduce_mean(tf.cast(correct_predictions, "float"), name="accuracy")

    def graph_conv_chebyshev(self, x, L, K, F_out):
        """
        Graph convolutional operation.
        """
        # K = Chebyshev polynomial order & support size
        # F_out = No. of output features (per vertex)
        # B = Batch size
        # V = No. of vertices
        # F_in = No. of input features (per vertex)
        B, V, F_in = x.get_shape()
        B, V, F_in = int(B), int(V), int(F_in)

        # Rescale Laplacian and store as a TF sparse tensor (copy to not modify the shared L)
        L = scipy.sparse.csr_matrix(L)
        L = graph.rescale_L(L, lmax=2)
        L = L.tocoo()
        indices = np.column_stack((L.row, L.col))
        L = tf.SparseTensor(indices, L.data, L.shape)
        L = tf.sparse_reorder(L)
        L = tf.cast(L, tf.float32)

        # Transform to Chebyshev basis
        x0 = tf.transpose(x, perm=[1, 2, 0])     # V x F_in x B
        x0 = tf.reshape(x0, [V, F_in * B])       # V x F_in*B
        x = tf.expand_dims(x0, 0)                # 1 x V x F_in*B

        def concat(x, x_):
            x_ = tf.expand_dims(x_, 0)           # 1 x V x F_in*B
            return tf.concat([x, x_], axis=0)    # K x V x F_in*B
        if K > 1:
            x1 = tf.sparse_tensor_dense_matmul(L, x0)
            x = concat(x, x1)
        for k in range(2, K):
            x2 = 2 * tf.sparse_tensor_dense_matmul(L, x1) - x0  # V x F_in*B
            x = concat(x, x2)
            x0, x1 = x1, x2
        x = tf.reshape(x, [K, V, F_in, B])       # K x V x F_in x B
        x = tf.transpose(x, perm=[3, 1, 2, 0])   # B x V x F_in x K
        x = tf.reshape(x, [B * V, F_in * K])     # B*V x F_in*K

        # Compose linearly F_in features to get F_out features
        W = tf.Variable(tf.truncated_normal([F_in * K, F_out], stddev=0.1), name="W")
        x = tf.matmul(x, W)                      # B*V x F_out
        x = tf.reshape(x, [B, V, F_out])         # B x V x F_out

        return x
